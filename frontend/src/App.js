import React, { useEffect, useState } from 'react'
import { useSearchParams } from 'react-router-dom'
import Box from '@mui/material/Box'
import Navbar from './components/navbar/navbar'
import LoadingScreen from './components/loadingScreen/loadingScreen'
import Map from './components/map/map'
import './index.css'
import { getPoints } from './apiCalls/points'
import { getLngAndLatFromOriginId } from './apiCalls/origins'
import { ORIGINAL_POINTS } from './components/map/dummyData'

function App() {
    const [points, setPoints] = useState(ORIGINAL_POINTS)
    const [searchParams, ] = useSearchParams()
    const [originId, ] = useState(getOriginId(searchParams))
    const [isLoading, setIsLoading] = useState(true)
    const [lng, setLng] = useState(11.615)
    const [lat, setLat] = useState(48.114)

    // Get points
    useEffect(() => {
        const fetchData = async () => {
            try {
                // Collect points
                const points = await getPoints(originId)
                setPoints(points)

                // Get center map position
                var [newLng, newLat] = await getLngAndLatFromOriginId(originId)
                setLng(newLng)
                setLat(newLat)
                setIsLoading(false)
            } catch (e) {
                alert(e)
            }
        }
        fetchData().catch(console.error)
    }, [originId])

    return (
        <Box height="100vh" display="flex" flexDirection="column">
            {isLoading && <LoadingScreen />}
            {!isLoading && <Navbar points={points}/>}
            {!isLoading && <Map points={points} lat={lat} lng={lng} />}
            {/* <LoadingScreen/> */}
        </Box>
    )
}

function getOriginId(searchParams) {
    return searchParams.get('origin-id') || 8
}

export default App
