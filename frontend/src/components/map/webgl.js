export const compile = (gl, vertexSource, fragmentSource) => {
    // Compile the vertex shader
    const vertexShader = gl.createShader(gl.VERTEX_SHADER)
    gl.shaderSource(vertexShader, vertexSource)
    gl.compileShader(vertexShader)

    // Compile the fragment shader
    const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER)
    gl.shaderSource(fragmentShader, fragmentSource)
    gl.compileShader(fragmentShader)

    // Create the WebGL program
    const program = gl.createProgram()
    gl.attachShader(program, vertexShader)
    gl.attachShader(program, fragmentShader)
    gl.linkProgram(program)

    // Log compilation errors, if any
    console.log('vertex shader:', gl.getShaderInfoLog(vertexShader) || 'OK')
    console.log('fragment shader:', gl.getShaderInfoLog(fragmentShader) || 'OK')
    console.log('program:', gl.getProgramInfoLog(program) || 'OK')

    return program
}
