import { compile } from './webgl'
import { getMaxDuration, generateDrawableData, generateVertice, generateColor, addColorInformation } from '../../apiCalls/points'

// Create GLSL source for vertex shader
const vertexSource = `
		uniform mat4 u_matrix;
		attribute vec3 position;
		attribute vec4 color;

		varying vec4 v_color;

		void main() {
		  gl_Position = u_matrix * vec4(position, 1.0);
			v_color = color;
			gl_PointSize = 20.0;
		}`

// Create GLSL source for fragment shader
const fragmentSource = `
		precision mediump float;

		varying vec4 v_color;

		void main() {
		    gl_FragColor = v_color;
		}`

export function createHeatMapLayer(points) {
    const heatMapLayer = {
        id: 'highlight',
        type: 'custom',
        points: points,

        // method fired on each animation frame
        render(gl, matrix) {
            gl.useProgram(this.program)

            // set transformation matrix
            gl.uniformMatrix4fv(gl.getUniformLocation(this.program, 'u_matrix'), false, matrix)

            // Use buffer for position
            this.position = gl.getAttribLocation(this.program, 'position')
            gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer)
            gl.vertexAttribPointer(this.position, 3, gl.FLOAT, false, 0, 0)
            gl.enableVertexAttribArray(this.position)

            // Use buffer for color
            this.color = gl.getAttribLocation(this.program, 'color')
            gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer)
            gl.vertexAttribPointer(this.color, 4, gl.FLOAT, false, 0, 0)
            gl.enableVertexAttribArray(this.color)

            // Draw
            gl.enable(gl.BLEND)
            gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
            gl.drawArrays(gl.TRIANGLES, 0, this.verticesLength)

            // Just for debugging
            // gl.drawArrays(gl.POINTS, 0, this.verticesLength);
        },
    }

    // method called when the layer is added to the map
    // Search for StyleImageInterface in https://maplibre.org/maplibre-gl-js/docs/API/
    heatMapLayer.onAdd = async function (map, gl) {
        this.program = compile(gl, vertexSource, fragmentSource)

        // Get the maximum value for color calculations
        var maxDuration = getMaxDuration(this.points)
        console.log('Found maximum duration from all points: ', maxDuration)

        addColorInformation(this.points)

        // Define vertices and colors
        const drawableData = generateDrawableData(this.points)
        this.verticesLength = drawableData.length
        const vertices = new Float32Array(drawableData.map(generateVertice(maxDuration)).flat())
        this.colors = new Float32Array(drawableData.map(generateColor).flat())

        // create and initialize a WebGLBuffer to store vertex and color data
        this.positionBuffer = gl.createBuffer()
        gl.bindBuffer(gl.ARRAY_BUFFER, this.positionBuffer)
        gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW)

        this.colorBuffer = gl.createBuffer()
        gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer)
        gl.bufferData(gl.ARRAY_BUFFER, this.colors, gl.STATIC_DRAW)
    }

    return heatMapLayer
}
