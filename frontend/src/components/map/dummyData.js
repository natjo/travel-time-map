// Points in lat/lng coordinates
// Just dummy points
import maplibregl from 'maplibre-gl'

const ORIGINAL_POINTS_RAW = [
    {
        coord: {
            lat: 48.106,
            lng: 11.613,
        },
        duration: 10,
    },
    {
        coord: {
            lat: 48.12,
            lng: 11.591,
        },
        duration: 20,
    },
    {
        coord: {
            lat: 48.115,
            lng: 11.636,
        },
        duration: 30,
    },
    {
        coord: {
            lat: 48.128,
            lng: 11.606,
        },
        duration: 16,
    },
    {
        coord: {
            lat: 48.105,
            lng: 11.582,
        },
        duration: 3,
    },
    {
        coord: {
            lat: 48.099,
            lng: 11.618,
        },
        duration: 5,
    },
    {
        coord: {
            lat: 48.126,
            lng: 11.627,
        },
        duration: 15,
    },
    {
        coord: {
            lat: 48.102,
            lng: 11.64,
        },
        duration: 27,
    },
    {
        coord: {
            lat: 48.108,
            lng: 11.656,
        },
        duration: 13,
    },
    {
        coord: {
            lat: 48.128,
            lng: 11.58,
        },
        duration: 1,
    },
    {
        coord: {
            lat: 48.1,
            lng: 11.565,
        },
        duration: 21,
    },
    {
        coord: {
            lat: 48.115,
            lng: 11.603,
        },
        duration: 21,
    },
]

const transformPointsToXY = (points) => {
    return points.map((p) => {
        return {
            pos: maplibregl.MercatorCoordinate.fromLngLat(p.coord),
            duration: p.duration,
        }
    })
}

export const ORIGINAL_POINTS = transformPointsToXY(ORIGINAL_POINTS_RAW)
