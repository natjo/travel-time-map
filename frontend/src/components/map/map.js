import React, { useRef, useEffect, useState } from 'react'
import maplibregl from 'maplibre-gl'
import 'maplibre-gl/dist/maplibre-gl.css'
import './map.css'
import { createHeatMapLayer } from './heatmapLayer'

// props parameters
// lng -    longitude the the map should center on
// lat -    latitude the the map should center on
// points - all points the heatmap should display on top of the map
export default function Map(props) {
    const maptilerKey = 'SX9HbcqKjcPuUNjtYNLf'
    const mapContainer = useRef(null)
    const map = useRef(null)
    const [lng] = useState(props.lng)
    const [lat] = useState(props.lat)
    const [zoom] = useState(12)
    const [API_KEY] = useState(maptilerKey)
    const [points] = useState(props.points)

    useEffect(() => {
        if (map.current) return // stops map from intializing more than once

        map.current = new maplibregl.Map({
            container: mapContainer.current,
            style: `https://api.maptiler.com/maps/streets-v2/style.json?key=${API_KEY}`,
            center: [lng, lat],
            zoom: zoom,
            antialias: true, // create the gl context with MSAA antialiasing, so custom layers are antialiased
        })
        map.current.addControl(new maplibregl.NavigationControl(), 'top-right')

        // create a custom style layer to implement the WebGL content
        // add the custom style layer to the map
        map.current.on('load', () => {
            const heatMapLayer = createHeatMapLayer(points)
            map.current.addLayer(heatMapLayer)
        })
    }, [API_KEY, lng, lat, zoom, points])

    return (
        <div className="map-wrap">
            <div ref={mapContainer} className="map" />
        </div>
    )
}
