import React from 'react'
import Box from '@mui/material/Box'
import LinearProgress from '@mui/material/LinearProgress'

export default function Navbar(props) {
    return (
        <Box
            sx={{
                p: 8,
                bgcolor: 'grey.300'
            }}
            flex={1}
            overflow="auto"
        >
            <h1>Data is being loaded...</h1>
            <LinearProgress color="secondary" />
            <LinearProgress color="success" />
            <LinearProgress color="inherit" />
        </Box>
    )
}
