import React from 'react'
import { useSearchParams } from 'react-router-dom'
import Button from '@mui/material/Button'

export default function OriginSelector() {
    const [, setSearchParams] = useSearchParams()
    const onClick = () => {
        setSearchParams({ 'origin-id': 4 })
        window.location.reload(false)
    }
    return (
            <Button variant="outlined" onClick={onClick}>
                Set origin id 4
            </Button>
    )
}
