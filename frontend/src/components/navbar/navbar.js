import React from 'react'
import Box from '@mui/material/Box'
import Legend from './legend/legend'
import OriginSelector from './originSelector/originSelector'

export default function Navbar(props) {
    return (
        <Box
            sx={{
                py: 1,
                px: 8,
                color: 'grey.200',
                bgcolor: 'grey.900',
            }}
            display="flex"
            justifyContent="space-between"
        >
            <Legend points={props.points} />
            <h1>Travel Time Map</h1>
            <OriginSelector/>
        </Box>
    )
}
