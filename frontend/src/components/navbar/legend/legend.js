import Box from '@mui/material/Box'
import React from 'react'
import { getMaxDuration, addColorInformation, colorInSegment } from '../../../apiCalls/points'

export default function Legend(props) {
    var segmentLimits = addColorInformation(props.points)
    var maxDuration = getMaxDuration(props.points)
    console.log('Segment limits for duration are: ', segmentLimits)

    // Create the colorBoxes
    // Slightly adapted version from addColorInformationSegments in data.js
    const colorBoxes = []
    var segment = 0
    for (let d = 0; d <= maxDuration; d += maxDuration / 255) {
        if (d >= segmentLimits[segment + 1]) {
            segment++
        }
        var Y = (d - segmentLimits[segment]) / (segmentLimits[segment + 1] - segmentLimits[segment])
        let color = colorInSegment(Y, segment)
        let r = Math.ceil(color[0] * 99)
            .toFixed(0)
            .padStart(2, '0')
        let g = Math.ceil(color[1] * 99)
            .toFixed(0)
            .padStart(2, '0')
        let b = Math.ceil(color[2] * 99)
            .toFixed(0)
            .padStart(2, '0')
        let backgroundColor = '#' + r + g + b
        colorBoxes.push(
            <Box
                key={backgroundColor}
                sx={{
                    width: '2px',
                    backgroundColor: backgroundColor,
                }}
            />
        )
    }

    // Create the labels
    var colorLabels = []
    for (const segmentLimit of segmentLimits) {
        colorLabels.push(<p key={segmentLimit}>{Math.round(segmentLimit / 60).toFixed(0)}m</p>)
    }

    return (
        <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <Box sx={{ display: 'flex', flexDirection: 'row', flexGrow: 1 }}>{colorBoxes}</Box>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}
            >
                {colorLabels}
            </Box>
        </Box>
    )
}
