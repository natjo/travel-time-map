export const DATA_URL = 'http://0.0.0.0:8090'
// We assume minimum duration is always 0
export const MIN_DURATION = 0
// const MAX_HEIGHT = 0.0001;
export const MAX_HEIGHT = 0
export const ALPHA = 0.4
export const USE_COLOR_SEGMENTS = true