import maplibregl from 'maplibre-gl'
import Delaunator from 'delaunator'
import {DATA_URL, MIN_DURATION, MAX_HEIGHT, ALPHA, USE_COLOR_SEGMENTS} from './config'


// Returns the points as a list with this structure:
// [
// 	{
// 		coord: { lng, lat},
// 		duration: duration
//	},
//	...
//]
export const getPoints = async (originId) => {
    var endpoint = 'api/travel-points'
    var url = new URL(`${DATA_URL}/${endpoint}`)

    url.searchParams.append('origin-id', originId)

    let response = await fetch(url)

    if (response.ok) {
        let rawPoints = await response.json()
        console.log(`Received ${rawPoints.length} points`)
        return transformPointsToXY(rawPoints)
    } else {
        alert('HTTP-Error: ' + response.status)
    }
}

export const getMaxDuration = (points) => {
    return Math.max(...points.map((p) => p.duration))
}

const transformPointsToXY = (points) => {
    return points.map((p) => {
        return {
            pos: maplibregl.MercatorCoordinate.fromLngLat(p.coord),
            duration: p.duration,
        }
    })
}

// Returns a function that generates the correct point with a predefined maximum duration
export const generateVertice = (maxDuration) => {
    return (p) => {
        var pos = p.pos
        var z = calculateHeight(p.duration, maxDuration)
        return [pos.x, pos.y, z]
    }
}

const calculateHeight = (duration, maxDuration) => {
    return (MAX_HEIGHT * (duration - MIN_DURATION)) / (maxDuration - MIN_DURATION)
}

// Creates a new list of points rearranged and duplicated to be used in the WebGL rendering
export const generateDrawableData = (data) => {
    const delaunay = Delaunator.from(
        data,
        (p) => p.pos.x,
        (p) => p.pos.y
    )
    var out = []
    delaunay.triangles.forEach((trianglePointIndex) => {
        out.push(data[trianglePointIndex])
    })
    return out
}

export const generateColor = (p) => {
    return p.color
}

// Adds to each point in the provided list the points.color attribute, which consists of [r,g,b,alpha]
export const addColorInformation = (points) => {
    if (USE_COLOR_SEGMENTS) {
        return addColorInformationSegments(points)
    } else {
        addColorInformationLinear(points)
    }
}

const addColorInformationLinear = (points) => {
    var maxDuration = getMaxDuration(points)
    for (var p of points) {
        var f = p.duration / maxDuration
        // Multiply by 6 even though we only use 5 segments, as the highest number never is shown
        var a = f * 6
        // This boosts lower numbers and decreases higher numbers. Otherwise most of the colors would be the higher number
        a = Math.pow(0.054 * a, 3) - Math.pow(0.496 * a, 2) + 2.025 * a
        var segment = Math.floor(a)
        var Y = a - segment
        p.color = colorInSegment(Y, segment)
    }
}

const addColorInformationSegments = (points) => {
    // Find segments
    points.sort((a, b) => {
        return a.duration - b.duration
    })
    var segmentLimits = [
        points[Math.floor((points.length * 0) / 6)].duration,
        points[Math.floor((points.length * 1) / 6)].duration,
        points[Math.floor((points.length * 2) / 6)].duration,
        points[Math.floor((points.length * 3) / 6)].duration,
        points[Math.floor((points.length * 4) / 6)].duration,
        points[Math.floor((points.length * 5) / 6)].duration,
        points[Math.floor((points.length * 6) / 6) - 1].duration,
    ]

    var segment = 0
    for (var p of points) {
        if (p.duration >= segmentLimits[segment + 1]) {
            segment++
        }
        var Y = (p.duration - segmentLimits[segment]) / (segmentLimits[segment + 1] - segmentLimits[segment])
        p.color = colorInSegment(Y, segment)
    }

    return segmentLimits
}

export const colorInSegment = (Y, segment) => {
    var r = 0
    var g = 0
    var b = 0
    var alpha = ALPHA
    switch (segment) {
        case 0:
            // yellow to green
            r = 1 - Y
            g = 1
            b = 0
            break
        case 1:
            // green to cyan
            r = 0
            g = 1
            b = Y
            break
        case 2:
            // cyan to blue
            r = 0
            g = 1 - Y
            b = 1
            break
        case 3:
            // blue to violett
            r = Y
            g = 0
            b = 1
            break
        case 4:
            // violett to red
            r = 1
            g = 0
            b = 1 - Y
            break
        case 5:
            // red to black
            r = 1 - Y
            g = 0
            b = 0
            break
        case 6:
            // Will turn black since the init values are all 0
            break
        default:
            // Will turn black since the init values are all 0
            break
    }
    return [r, g, b, alpha]
}
