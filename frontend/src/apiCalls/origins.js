import {DATA_URL} from './config'

// Returns all possible origin names and their id
// [
// 	origin-name: id,
//	...
//]
export const getOriginInfo = async () => {
    var endpoint = 'api/origin-names'
    var url = new URL(`${DATA_URL}/${endpoint}`)

    let response = await fetch(url)

    if (response.ok) {
        return await response.json()
    } else {
        alert('HTTP-Error: ' + response.status)
    }
}
export const getLngAndLatFromOriginId = async (originId) => {
    var originInfo = await getOriginInfo()
    return [1,2]
}