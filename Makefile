.ONESHELL:
.PHONY: start-db run-debug-backend run-backend build-backend

start-db:
	docker-compose up

# Creates initial user and its database
# postgres://travel_time_info_user:secretpassword@localhost:5432/travel_time_info?sslmode=disable
init-db:
	docker exec -it travel-time-map_db_1 bash -c "psql << SQL
	CREATE DATABASE travel_time_info;
	CREATE USER travel_time_Info_user WITH ENCRYPTED PASSWORD 'secretpassword';
	GRANT ALL PRIVILEGES ON DATABASE travel_time_info TO travel_time_info_user;
	\c travel_time_info;
	GRANT ALL ON SCHEMA public TO travel_time_info_user;
	SQL
	"

reset-db-content:
	docker exec -it travel-time-map_db_1 bash -c "psql << SQL
	\c travel_time_info;
	DELETE FROM travel_times;
	DELETE FROM coords;
	SQL
	"

# Air adds hot reloading
run-debug-backend:
	cd ./backend && . ./.env && LOG_LEVEL='debug' air

run-backend: build-backend
	cd ./backend && . ./.env && LOG_LEVEL='info' air

build-backend:
	cd backend && go build -o backend main.go
