# Travel Time Map

Creates a map with a color overlay indicating travel times from a chosen location

# Add origins
The number of origin points to calculate travel time heat maps from is limited to entries in the `origins` table. To insert data in there, no api endpoint is provided. To keep things simple you can add points with this command:
```
docker exec -it travel-time-map_db_1 bash -c "psql << SQL
\c travel_time_info;
INSERT INTO origins (origin_coord, name, mode, grid_radius, grid_stepsize) VALUES (1234, '<human-readable-name>', '<mode>', 8000, 100) ON CONFLICT DO NOTHING;
SQL
"
```
- if `origin_coord` does not exist in the `coords` table yet, create it with e.g. `INSERT INTO coords (lat, lng) VALUES (43.21, 12.34) ON CONFLICT DO NOTHING;`
- `origin_coord` must be an id that exist in the `coords` table. You can run e.g. `SELECT * FROM coords WHERE lat = 43.21 AND lng = 12.34;`
- `mode` must be one of "BICYCLING", "WALKING", "DRIVING", "TRANSIT".
