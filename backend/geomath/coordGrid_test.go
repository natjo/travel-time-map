package geomath

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPosCloudCorrectNumElements(t *testing.T) {
	// Given
	coord := NewCoord(10, 10)
	radius := 500.0
	stepSize := 100.0

	// When
	result := GenerateCoordGrid(coord, radius, stepSize)

	// Then
	assert.Equal(t, 73, len(result), "Coord grid should have 73 elements, but has these:\n"+getStringOfGrid(result))
}

func TestPosCloudCorrectNumElementsOnlyCenter(t *testing.T) {
	// Given
	coord := NewCoord(10, 10)
	radius := 100.0
	stepSize := 200.0

	// When
	result := GenerateCoordGrid(coord, radius, stepSize)

	// Then
	assert.Equal(t, 1, len(result), "Coord grid should have 1 element, but has these:\n"+getStringOfGrid(result))
}

func TestPosCloudCorrectNumElementsSmall(t *testing.T) {
	// Given
	coord := NewCoord(10, 10)
	radius := 201.0
	stepSize := 200.0

	// When
	result := GenerateCoordGrid(coord, radius, stepSize)

	// Then
	assert.Equal(t, 5, len(result), "Coord grid should have 5 elements, but has these:\n"+getStringOfGrid(result))
}

func TestPosCloudCorrectNumElementsABitSmall(t *testing.T) {
	// Given
	coord := NewCoord(10, 10)
	radius := 401.0
	stepSize := 200.0

	// When
	result := GenerateCoordGrid(coord, radius, stepSize)

	// Then
	assert.Equal(t, 13, len(result), "Coord grid should have 5 elements, but has these:\n"+getStringOfGrid(result))
}

func TestPosCloudCenterOriginal(t *testing.T) {
	// Given
	centerCoord := NewCoord(10, 10)
	radius := 500.0
	stepSize := 100.0

	// When
	result := GenerateCoordGrid(centerCoord, radius, stepSize)

	// Then
	generatedCenterCoord := result[int(len(result)/2)]
	assert.Less(t, getLatDistance(centerCoord, generatedCenterCoord), stepSize)
	assert.Less(t, getLngDistance(centerCoord, generatedCenterCoord), stepSize)
}

func TestPosCloudCorrectDistance(t *testing.T) {
	// Given
	centerCoord := NewCoord(10, 10)
	radius := 500.0
	stepSize := 100.0

	// When
	result := GenerateCoordGrid(centerCoord, radius, stepSize)

	// Then
	nextToCenterCoord := result[int(len(result)/2)+1]
	assert.InDelta(t, centerCoord.Lat, nextToCenterCoord.Lat, 1e-4, "Lat should be the same")
	assert.InDelta(t, stepSize, getLngDistance(centerCoord, nextToCenterCoord), 0.5, "Lng distance should be the stepSize")
}

func getStringOfGrid(coords []*Coord) string {
	s := ""
	for _, c := range coords {
		s = s + c.ToString() + "\n"
	}
	return s
}
