package geomath

func GenerateCoordGrid(gridCenter *Coord, radius, stepDistance float64) []*Coord {
	// Generates an array of coordinates with the center in the middle, all points together create a circle with a maximum radius as requested
	// Tries to find close coordinates for the center in the database to save on routing queries
	gridCenter = getExistingCloseCenter(gridCenter)
	var coordGrid []*Coord

	// Iterate first over all vertical steps
	for y := 0; true; y++ {
		// Add horizontal line of the circle at current vertical position
		horizontalCoordsTop := addHorizontalCoords(gridCenter, y, radius, stepDistance)
		if len(horizontalCoordsTop) == 0 {
			// nothing in the horizontal line means we are completely out of the radius
			break
		}
		coordGrid = append(coordGrid, horizontalCoordsTop...)

		// y equal 0 means we are in the center of the circle, here the line below and above are the same
		if y != 0 {
			horizontalCoordsBot := addHorizontalCoords(gridCenter, -y, radius, stepDistance)
			coordGrid = append(horizontalCoordsBot, coordGrid...)
		}
	}
	return coordGrid
}

func addHorizontalCoords(gridCenter *Coord, stepsVertical int, radius, stepDistance float64) []*Coord {
	var coordGrid []*Coord
	gridVerticalLineCoord := addDistanceToCoord(gridCenter, float64(stepsVertical)*stepDistance, BEARING_LNG)

	// First add the point on the centered vertical line directly.
	// We can't do that in the loop as this point at x=0 would be added twice
	if getDistance(gridCenter, gridVerticalLineCoord) <= radius {
		coordGrid = append(coordGrid, gridVerticalLineCoord)
	} else {
		return coordGrid
	}

	for x := 1; true; x++ {
		coordRight := addDistanceToCoord(gridVerticalLineCoord, float64(x)*stepDistance, BEARING_LAT)
		if getDistance(gridCenter, coordRight) <= radius {
			coordGrid = append(coordGrid, coordRight)
			coordLeft := addDistanceToCoord(gridVerticalLineCoord, -1*float64(x)*stepDistance, BEARING_LAT)
			coordGrid = append([]*Coord{coordLeft}, coordGrid...)
		} else {
			break
		}
	}
	return coordGrid
}
