package geomath

import (
	"math"
)

// These are helpful to move a distance from a coordinate in latitude or longitude solely
const BEARING_LNG float64 = 0

var BEARING_LAT float64 = toRadians(90)

const EARTH_RADIUS float64 = 6371e3

func getDistance(coord0, coord1 *Coord) float64 {
	// Returns the distance in meters
	return haversine(coord0, coord1)
}

func getLngDistance(coord0, coord1 *Coord) float64 {
	// Returns the distance in meters
	return haversine(coord0, NewCoord(coord0.Lat, coord1.Lng))
}

func getLatDistance(coord0, coord1 *Coord) float64 {
	// Returns the distance in meters
	return haversine(coord0, NewCoord(coord1.Lat, coord0.Lng))
}

func addDistanceToCoord(coord *Coord, distance, bearing float64) *Coord {
	// distance is in meters
	delta := distance / EARTH_RADIUS
	lat0 := toRadians(coord.Lat)
	lng0 := toRadians(coord.Lng)
	lat1 := math.Asin(math.Sin(lat0)*math.Cos(delta) + math.Cos(lat0)*math.Sin(delta)*math.Cos(bearing))
	lng1 := lng0 + math.Atan2(math.Sin(bearing)*math.Sin(delta)*math.Cos(lat0), math.Cos(delta)-math.Sin(lat0)*math.Sin(lat1))
	lat1 = toDegrees(lat1)
	lng1 = toDegrees(lng1)
	return NewCoord(lat1, lng1)
}

func haversine(coord0 *Coord, coord1 *Coord) float64 {
	// distance is in meters
	deltaLat := toRadians(coord1.Lat - coord0.Lat)
	deltaLng := toRadians(coord1.Lng - coord0.Lng)
	lat0 := toRadians(coord0.Lat)
	lat1 := toRadians(coord1.Lat)
	sinLat := math.Pow(math.Sin(deltaLat/2), 2)
	sinLng := math.Pow(math.Sin(deltaLng/2), 2)
	a := sinLat + math.Cos(lat0)*math.Cos(lat1)*sinLng
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return EARTH_RADIUS * c
}

func getExistingCloseCenter(pos *Coord) *Coord {
	// TODO query database for possible closest center
	return pos
}

func arrangeCoord(pos *Coord, gridStepsize float64) *Coord {
	// We span a grid over the world, this function returns the closest position in the grid to the provided position
	// Get lat and lng distance to 0
	latDistance := getLatDistance(pos, NewCoord(0, 0))
	lngDistance := getLngDistance(pos, NewCoord(0, 0))

	// Get new lng and lat on grid
	latDiff := getClostestNumber(latDistance, gridStepsize) - latDistance
	lngDiff := getClostestNumber(lngDistance, gridStepsize) - lngDistance

	// Return pos with new lng and lat on grid
	pos = addDistanceToCoord(pos, latDiff, BEARING_LAT)
	pos = addDistanceToCoord(pos, lngDiff, BEARING_LNG)

	return pos

}

func getClostestNumber(p, gridStepsize float64) float64 {
	gridDistance := gridStepsize
	diff := math.Mod(p, gridDistance)
	if diff > gridDistance/2 {
		return p - diff + gridDistance
	} else {
		return p - diff
	}
}
