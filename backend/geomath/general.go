package geomath

import "math"

func toRadians(degrees float64) float64 {
	return degrees * (math.Pi / 180)
}

func toDegrees(radians float64) float64 {
	return radians * (180.0 / math.Pi)
}

func roundPrecision(x, unit float64) float64 {
	return math.Round(x/unit) * unit
}
