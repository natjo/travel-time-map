package geomath

import "fmt"

// The algorithms used for GPS operations are not too precise
const MAX_PRECISION float64 = 1e-5

type Coord struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

func NewCoord(lat, lng float64) *Coord {
	return &Coord{
		Lat: roundPrecision(lat, MAX_PRECISION),
		Lng: roundPrecision(lng, MAX_PRECISION),
	}
}

func (coord *Coord) ToString() string {
	return fmt.Sprintf("(%v, %v)", coord.Lat, coord.Lng)
}
