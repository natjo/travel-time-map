package geomath

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetDistanceSameCoordinates(t *testing.T) {
	// Given
	coord0 := NewCoord(1, 1)

	// When
	result := getDistance(coord0, coord0)

	// Then
	assert.Equal(t, 0.0, result, "Distance between same coordinates should be 0")
}

func TestGetDistance(t *testing.T) {
	// Given
	coord0 := NewCoord(0, 0)
	coord1 := NewCoord(45, 45)

	// When
	result := getDistance(coord0, coord1)

	// Then
	assert.InDelta(t, 6.672e6, result, 5e2)
}

func TestGetLngDistance(t *testing.T) {
	// Given
	coord0 := NewCoord(0, 0)
	coord1 := NewCoord(45, 45)

	// When
	result := getLngDistance(coord0, coord1)

	// Then
	assert.InDelta(t, 5.0037717e6, result, 5e2)
}

func TestGetLatDistance(t *testing.T) {
	// Given
	coord0 := NewCoord(0, 0)
	coord1 := NewCoord(20, 20)

	// When
	result := getLatDistance(coord0, coord1)

	// Then
	assert.InDelta(t, 2.22389853e6, result, 5e2)
}

func TestAddLatDistanceToCoord0(t *testing.T) {
	// Given
	coord0 := NewCoord(45, 45)
	distance := 100e3

	// When
	result := addDistanceToCoord(coord0, distance, BEARING_LAT)

	// Then
	// 44.9930556°, -043.7283333°
	expected := NewCoord(44.9927778, 46.1655)
	assert.InDelta(t, expected.Lat, result.Lat, 1.1e-1)
	assert.InDelta(t, expected.Lng, result.Lng, 1.1e-1)
}

func TestAddLatDistanceToCoord1(t *testing.T) {
	// Given
	coord0 := NewCoord(10, 10)
	distance := 100e3

	// When
	result := addDistanceToCoord(coord0, distance, BEARING_LAT)

	// Then

	expected := NewCoord(9.9994, 10.912)
	assert.InDelta(t, expected.Lat, result.Lat, 1e-2)
	assert.InDelta(t, expected.Lng, result.Lng, 1e-2)
}

func TestAddLatDistanceToCoord2(t *testing.T) {
	// Given
	coord0 := NewCoord(52.20472, 0.14056)
	distance := 15e3

	// When
	result := addDistanceToCoord(coord0, distance, BEARING_LAT)

	// Then

	expected := NewCoord(52.20444, 0.36056)
	assert.InDelta(t, expected.Lat, result.Lat, 1e-2)
	assert.InDelta(t, expected.Lng, result.Lng, 1e-2)
}

func TestAddNegLatDistanceToCoord(t *testing.T) {
	// Given
	coord0 := NewCoord(44.9927778, 46.1655)
	distance := -100e3

	// When
	result := addDistanceToCoord(coord0, distance, BEARING_LAT)

	// Then
	expected := NewCoord(45, 45)
	assert.InDelta(t, expected.Lat, result.Lat, 1.1e-1)
	assert.InDelta(t, expected.Lng, result.Lng, 1.1e-1)
}

func TestAddLngDistanceToCoord(t *testing.T) {
	// Given
	coord0 := NewCoord(45, 45)
	distance := 100e3

	// When
	result := addDistanceToCoord(coord0, distance, BEARING_LNG)

	// Then
	expected := NewCoord(45.9063889, 45.0)
	assert.InDelta(t, expected.Lat, result.Lat, 1e-1)
	assert.InDelta(t, expected.Lng, result.Lng, 1e-1)
}

func TestArrangeCoordNull(t *testing.T) {
	// Given
	gridStepsize := float64(100)
	coord0 := NewCoord(0, 0)

	// When
	result := arrangeCoord(coord0, gridStepsize)

	// Then
	assert.Equal(t, coord0.Lat, result.Lat)
	assert.Equal(t, coord0.Lng, result.Lng)
}

func TestArrangeCoordAlmostNull(t *testing.T) {
	// Given
	gridStepsize := float64(100)
	coord0 := NewCoord(1e-5, 1e-5)

	// When
	result := arrangeCoord(coord0, gridStepsize)

	// Then
	expected := NewCoord(0, 0)
	assert.Equal(t, expected.Lat, result.Lat)
	assert.Equal(t, expected.Lng, result.Lng)
}

func TestGetClosestNumber(t *testing.T) {
	// Given
	gridStepsize := float64(100)
	p := gridStepsize + gridStepsize/3

	// When
	result := getClostestNumber(p, gridStepsize)

	// Then
	assert.Equal(t, result, gridStepsize)
}
