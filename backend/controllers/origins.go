package controllers

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/travel-time-map/db"
	"gitlab.com/natjo/travel-time-map/geomath"
)

func GetOriginNames(db db.DBer) (map[int]string, error) {
	originNames, err := db.GetOriginNames()
	if err != nil {
		logrus.Errorf("Failed to query DB for origin points: %s", err)
		return nil, err
	}
	return originNames, nil
}

func GetOriginPositions(db db.DBer) (map[int]*geomath.Coord, error) {
	originPositions, err := db.GetOriginPositions()
	if err != nil {
		logrus.Errorf("Failed to query DB for origin points: %s", err)
		return nil, err
	}
	return originPositions, nil
}
