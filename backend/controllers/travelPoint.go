package controllers

import (
	"fmt"
	"math/rand"

	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/travel-time-map/config"
	"gitlab.com/natjo/travel-time-map/db"
	"gitlab.com/natjo/travel-time-map/geomath"
	"gitlab.com/natjo/travel-time-map/services"
)

type TravelPoint struct {
	Coord    *geomath.Coord `json:"coord"`
	Duration int            `json:"duration"`
}

func NewTravelPoint(coord *geomath.Coord, duration int) *TravelPoint {
	return &TravelPoint{
		Coord:    coord,
		Duration: duration,
	}
}

func GetTravelPoints(originId int, db db.DBer, googleMapsClient services.GoogleMapsClienter) ([]*TravelPoint, error) {
	centerCoord, err := GetCenterCoord(originId, db)
	if err != nil {
		return nil, fmt.Errorf("could not find original coordinate with id %v in database", originId)
	}
	travelMode, gridStepSize, gridRadius, err := db.GetOriginInformation(originId)
	if err != nil {
		return nil, err
	}
	logrus.Infof("Returning travel points for coordinate %v in radius %vm and step size of %vm", centerCoord.ToString(), gridRadius, gridStepSize)
	coordGrid := geomath.GenerateCoordGrid(centerCoord, float64(gridRadius), float64(gridStepSize))
	travelPoints := []*TravelPoint{NewTravelPoint(centerCoord, 0)}

	// List of coordinates not in the db yet
	coordsToBeLookedUp := []*geomath.Coord{}

	for _, coord := range coordGrid {
		duration, existsInDB := getDurationFromDB(centerCoord, coord, db, travelMode, config.DEPARTURE_TIME)
		if existsInDB {
			travelPoints = append(travelPoints, NewTravelPoint(coord, duration))
		} else {
			coordsToBeLookedUp = append(coordsToBeLookedUp, coord)
		}
	}
	logrus.Infof(
		"Returning travel points: %v looking up from the DB, %v requesting from the API",
		len(travelPoints),
		len(coordsToBeLookedUp),
	)
	if len(coordsToBeLookedUp) > 0 {
		if config.USE_DUMMY_DURATIONS {
			for _, coord := range coordsToBeLookedUp {
				travelPoints = append(travelPoints, NewTravelPoint(coord, getDummyDuration()))
				db.AddTravelTime(centerCoord, coord, int(getDummyDuration()), getDummyDuration(), travelMode, config.DEPARTURE_TIME)
			}
		} else {
			newTravelPoints := getDurationsFromAPI(centerCoord, coordsToBeLookedUp, db, googleMapsClient, travelMode, config.DEPARTURE_TIME)
			travelPoints = append(travelPoints, newTravelPoints...)
		}
	}
	logrus.Infof("Finished look up")

	return travelPoints, nil
}

func GetCenterCoord(originId int, db db.DBer) (*geomath.Coord, error) {
	centerCoordId, err := db.GetOriginCoordId(originId)
	if err != nil {
		return nil, err
	}
	centerCoord, err := db.GetCoord(centerCoordId)
	if err != nil {
		return nil, err
	}
	return centerCoord, err
}

func getDurationFromDB(p0, p1 *geomath.Coord, db db.DBer, travelMode string, departureTime int) (int, bool) {
	duration, durationExists, err := db.GetTravelTime(p0, p1, travelMode, departureTime)
	if err != nil {
		logrus.Fatal(err)
	}
	return duration, durationExists
}

func getDurationsFromAPI(
	originCoord *geomath.Coord,
	destinationCoords []*geomath.Coord,
	db db.DBer,
	googleMapsClient services.GoogleMapsClienter,
	travelMode string,
	departureTime int) []*TravelPoint {
	travelPoints := []*TravelPoint{}

	chunkedDestinationCoords := chunkSlice(destinationCoords, services.MAX_GOOGLE_MAPS_COORDS_PER_REQUEST)

	for _, destinationCoordsChunk := range chunkedDestinationCoords {
		durations, distances, err := googleMapsClient.GetDistanceMatrix(originCoord, destinationCoordsChunk, travelMode, departureTime)
		if err != nil {
			logrus.Fatal(err)
		}
		for i, coord := range destinationCoordsChunk {
			logrus.Infof("Add coord with %v duration, %v distance, at %v time to database", int(durations[i].Seconds()), distances[i], departureTime)
			err := db.AddTravelTime(originCoord, coord, int(durations[i].Seconds()), distances[i], travelMode, departureTime)
			if err != nil {
				logrus.Warnf("Couldn't add received travel time to database: %v", err)
			}
			travelPoints = append(travelPoints, NewTravelPoint(coord, int(durations[i])))
		}
	}
	return travelPoints
}

func chunkSlice(coords []*geomath.Coord, chunkSize int) [][]*geomath.Coord {
	chunkedCoords := [][]*geomath.Coord{}
	currentChunk := []*geomath.Coord{}
	for _, coord := range coords {
		currentChunk = append(currentChunk, coord)
		if len(currentChunk) == chunkSize {
			chunkedCoords = append(chunkedCoords, currentChunk)
			currentChunk = []*geomath.Coord{}
		}
	}
	if len(currentChunk) > 0 {
		chunkedCoords = append(chunkedCoords, currentChunk)
	}
	return chunkedCoords
}

func getDummyDuration() int {
	var max_duration float64 = 30
	duration := rand.Float64() * max_duration
	return int(duration)
}
