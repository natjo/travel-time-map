package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/natjo/travel-time-map/db"
	"gitlab.com/natjo/travel-time-map/server/routes"
	"gitlab.com/natjo/travel-time-map/services"
)

type Server struct {
	router *gin.Engine
}

func NewServer(db db.DBer, googleMapsClient services.GoogleMapsClienter) *Server {
	s := &Server{}
	s.setupRouter(db, googleMapsClient)
	return s
}

func (s *Server) Run(addr string) {
	s.router.Run(addr)
}

func (s *Server) setupRouter(db db.DBer, googleMapsClient services.GoogleMapsClienter) {
	s.router = gin.Default()
	s.initializeRoutes(db, googleMapsClient)
}

func (s *Server) initializeRoutes(db db.DBer, googleMapsClient services.GoogleMapsClienter) {
	routes.AddRoutesTravelPoints(s.router, db, googleMapsClient)
	routes.AddOriginPoints(s.router, db)
}
