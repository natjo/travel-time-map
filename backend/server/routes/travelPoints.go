package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/travel-time-map/controllers"
	"gitlab.com/natjo/travel-time-map/db"
	"gitlab.com/natjo/travel-time-map/services"
)

func AddRoutesTravelPoints(router *gin.Engine, db db.DBer, googleMapsClient services.GoogleMapsClienter) {
	env := &envTP{db: db, googleMapsClient: googleMapsClient}
	router.GET("/api/travel-points", env.getTravelPoints)
}

// Actual definition of routes
type envTP struct {
	db               db.DBer
	googleMapsClient services.GoogleMapsClienter
}

// Returns the points as a list with this structure:
// [
//
//	{
//		coord: { lng, lat},
//		duration: duration
//	},
//	...
//
// ]
// duration stands for the travel time in seconds to this location
func (env *envTP) getTravelPoints(c *gin.Context) {
	// Collect query parameters
	originId, err := getQueryInt("origin-id", c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Act on request
	points, err := controllers.GetTravelPoints(originId, env.db, env.googleMapsClient)

	// Send response
	if err != nil {
		logrus.Errorf("on getTravelPoints: %v", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": "internal error"})
	}
	c.JSON(http.StatusOK, points)
}
