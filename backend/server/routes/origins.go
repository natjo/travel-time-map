package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/natjo/travel-time-map/controllers"
	"gitlab.com/natjo/travel-time-map/db"
)

func AddOriginPoints(router *gin.Engine, db db.DBer) {
	env := &envOP{db: db}
	router.GET("/api/origin-names", env.getOriginNames)
}

type envOP struct {
	db db.DBer
}

// Returns a dictionary of name:id for each origin possible to choose from
func (env *envOP) getOriginNames(c *gin.Context) {
	// Act on request
	points, err := controllers.GetOriginNames(env.db)

	// Send response
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "internal error"})
	}
	c.JSON(http.StatusOK, points)
}
