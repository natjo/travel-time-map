package routes

import (
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// func getQueryFloat64(key string, c *gin.Context) (float64, error) {
// 	queryString, ok := c.GetQuery(key)
// 	if !ok {
// 		return 0, &QueryParameterError{msg: fmt.Sprintf("%s query value is required", key)}
// 	}
// 	val, err := strconv.ParseFloat(queryString, 64)
// 	if err != nil {
// 		logrus.Println(err.Error())
// 		return 0, &QueryParameterError{msg: fmt.Sprintf("invalid query value \"%s\", must be a number", queryString)}
// 	}
// 	return float64(val), nil
// }

// func getQueryFloat32(key string, c *gin.Context) (float32, error) {
// 	queryString, ok := c.GetQuery(key)
// 	if !ok {
// 		return 0, &QueryParameterError{msg: fmt.Sprintf("%s query value is required", key)}
// 	}
// 	val, err := strconv.ParseFloat(queryString, 32)
// 	if err != nil {
// 		logrus.Println(err.Error())
// 		return 0, &QueryParameterError{msg: fmt.Sprintf("invalid query value \"%s\", must be a number", queryString)}
// 	}
// 	return float32(val), nil
// }

func getQueryInt(key string, c *gin.Context) (int, error) {
	queryString, ok := c.GetQuery(key)
	if !ok {
		return 0, &QueryParameterError{msg: fmt.Sprintf("%s query value is required", key)}
	}
	val, err := strconv.Atoi(queryString)
	if err != nil {
		logrus.Println(err.Error())
		return 0, &QueryParameterError{msg: fmt.Sprintf("invalid query value \"%s\", must be an integer", queryString)}
	}
	return val, nil
}

type QueryParameterError struct {
	msg string
}

func (r *QueryParameterError) Error() string {
	return r.msg
}
