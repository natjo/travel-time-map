package services

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/travel-time-map/geomath"
	"googlemaps.github.io/maps"
)

const MAX_GOOGLE_MAPS_COORDS_PER_REQUEST = 25

type GoogleMapsClienter interface {
	GetDistanceMatrix(*geomath.Coord, []*geomath.Coord, string, int) ([]time.Duration, []int, error)
}

type GoogleMapsClient struct {
	c *maps.Client
}

func NewGoogleMapsClient(apiKey string) *GoogleMapsClient {
	client, err := maps.NewClient(maps.WithAPIKey(apiKey))
	if err != nil {
		logrus.Fatal(err)
	}
	return &GoogleMapsClient{c: client}
}

// Returns array of duration (in seconds) and distance (in meters) on success
// At most 25 destination coordinates allowed at once
func (client *GoogleMapsClient) GetDistanceMatrix(originCoord *geomath.Coord, destinationCoords []*geomath.Coord, mode string, departure_time int) ([]time.Duration, []int, error) {
	if len(destinationCoords) > MAX_GOOGLE_MAPS_COORDS_PER_REQUEST {
		return nil, nil, errors.New("too many destination coordinates provided")
	}

	request := &maps.DistanceMatrixRequest{
		Origins:       []string{coordToString(originCoord)},
		Destinations:  getDestinationStrings(destinationCoords),
		Mode:          maps.TravelModeWalking,
		DepartureTime: strconv.Itoa(departure_time),
	}

	durations := []time.Duration{}
	distances := []int{}
	response, err := client.c.DistanceMatrix(context.Background(), request)
	if err != nil {
		logrus.Fatal(err)
	}
	if len(response.Rows) > 1 {
		logrus.Warnf("Received more than one origin address on distance request, ignoring all but first")
	}
	for i, row := range response.Rows {
		logrus.Debugf("OriginAdress: %v", response.OriginAddresses[i])
		logrus.Debugf("Received %v distances&durations from request", len(row.Elements))
		for _, element := range row.Elements {
			// Should always be just one element, as we always provide just one origin
			// debugPrintRowElement(element, response.DestinationAddresses[j])
			durations = append(durations, element.Duration)
			distances = append(distances, element.Distance.Meters)
		}
	}
	return durations, distances, nil
}

// To ensure we always produce the correct string: lat,lng
// We use this function to keep control
func coordToString(coord *geomath.Coord) string {
	return fmt.Sprintf("%v,%v", coord.Lat, coord.Lng)
}

func getDestinationStrings(coords []*geomath.Coord) []string {
	coordStrings := []string{}
	for _, coord := range coords {
		coordStrings = append(coordStrings, coordToString(coord))
	}
	return coordStrings
}

func debugPrintRowElement(element *maps.DistanceMatrixElement, destinationAddress string) {
	logrus.Debug("-----------")
	logrus.Debugf("DestinationAddress: %v", destinationAddress)
	logrus.Debugf("Status: %v", element.Status)
	logrus.Debugf("Duration: %v", element.Duration)
	logrus.Debugf("DurationInTraffic: %v", element.DurationInTraffic)
	logrus.Debugf("Distance: %vm", element.Distance.Meters)
}
