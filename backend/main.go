package main

import (
	"fmt"

	"gitlab.com/natjo/travel-time-map/config"
	"gitlab.com/natjo/travel-time-map/db"
	"gitlab.com/natjo/travel-time-map/logging"
	"gitlab.com/natjo/travel-time-map/server"
	"gitlab.com/natjo/travel-time-map/services"
)

func main() {
	// Init
	config.Init()
	logging.InitLogging()
	googleMapsClient := services.NewGoogleMapsClient(config.GOOGLE_MAPS_API_KEY)
	db := db.NewDB(config.DB_URI)
	defer db.Close()

	server := server.NewServer(db, googleMapsClient)

	// Run server
	fmt.Printf("Start server at %s content", config.SERVER_ADDR)
	server.Run(config.SERVER_ADDR)
}
