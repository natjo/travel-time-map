package config

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

var SERVER_ADDR string
var DB_URI string
var GOOGLE_MAPS_API_KEY string
var DEPARTURE_TIME int

// Just useful for debugging
const USE_DUMMY_DURATIONS bool = false

func Init() {
	SERVER_ADDR = getEnvWithDefault("SERVER_ADDR", "0.0.0.0:8090")
	DB_URI = getEnv("DB_URI")
	GOOGLE_MAPS_API_KEY = getEnv("GOOGLE_MAPS_API_KEY")

	// 9:00:00 - 8.12.2023 in Berlin
	DEPARTURE_TIME = int(time.Date(2023, 12, 8, 9, 0, 0, 0, time.UTC).Unix())
}

func getEnvWithDefault(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func getEnv(key string) string {
	value, ok := os.LookupEnv(key)
	if !ok {
		logrus.Fatalf("Missing argument \"%v\"", key)
	}
	return value
}
