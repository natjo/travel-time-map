package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"gitlab.com/natjo/travel-time-map/geomath"
)

type DB struct {
	db *sql.DB
}

func NewDB(dbUri string) *DB {
	db := connectDatabase(dbUri)
	initDatabase(db)
	return &DB{db}
}

type DBer interface {
	Close()
	AddCoord(*geomath.Coord) error
	GetCoord(int) (*geomath.Coord, error)
	AddTravelTime(*geomath.Coord, *geomath.Coord, int, int, string, int) error
	GetTravelTime(*geomath.Coord, *geomath.Coord, string, int) (int, bool, error)
	GetOriginNames() (map[int]string, error)
	GetOriginPositions() (map[int]*geomath.Coord, error)
	GetOriginCoordId(int) (int, error)
	GetOriginInformation(int) (string, int, int, error)
}

func (db *DB) GetOriginNames() (map[int]string, error) {
	query := `SELECT origins.id, origins.name FROM origins;`
	rows, err := db.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Extract values
	originNames := map[int]string{}
	for rows.Next() {
		var id int
		var name string
		err := rows.Scan(&id, &name)
		if err != nil {
			return nil, err
		}
		originNames[id] = name
	}
	return originNames, nil
}

func (db *DB) GetOriginPositions() (map[int]*geomath.Coord, error) {
	query := `SELECT (origins.id, lat, lng) FROM coords INNER JOIN origins ON coords.id = origins.origin_coord;`
	rows, err := db.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Extract values
	originPositions := map[int]*geomath.Coord{}
	for rows.Next() {
		var id int
		var lat float64
		var lng float64
		err := rows.Scan(&id, &lat, &lng)
		if err != nil {
			return nil, err
		}
		originPositions[id] = geomath.NewCoord(lat, lng)
	}
	return originPositions, nil
}

func (db *DB) GetOriginCoordId(originId int) (int, error) {
	var coordId int
	query := `SELECT origin_coord FROM origins WHERE id = $1;`
	err := db.db.QueryRow(query, originId).Scan(&coordId)
	if err != nil {
		return 0, err
	}
	return coordId, nil
}

func (db *DB) GetOriginInformation(originId int) (string, int, int, error) {
	var mode string
	var gridStepsize, gridRadius int
	query := `SELECT mode, grid_stepsize, grid_radius FROM origins WHERE id = $1;`
	err := db.db.QueryRow(query, originId).Scan(&mode, &gridStepsize, &gridRadius)
	if err != nil {
		return "", 0, 0, err
	}
	return mode, gridStepsize, gridRadius, nil
}

// Returns travel time if exists
// Second parameter indicates if a travel time from the specified parameters exists
func (db *DB) GetTravelTime(originCoord, targetCoord *geomath.Coord, mode string, departure_time int) (int, bool, error) {
	// Get coordinate IDs
	originCoordID, err := db.getOrAddCoordID(originCoord)
	if err != nil {
		return 0, false, err
	}
	targetCoordID, err := db.getOrAddCoordID(targetCoord)
	if err != nil {
		return 0, false, err
	}

	// Run actual request
	var duration int
	query := `SELECT duration FROM travel_times WHERE origin_coord = $1 AND target_coord = $2 AND mode = $3 AND departure_time = $4;`
	err = db.db.QueryRow(query, originCoordID, targetCoordID, mode, departure_time).Scan(&duration)
	if err != nil {
		if err != sql.ErrNoRows {
			// a real error happened!
			return 0, false, err
		}
		return 0, false, nil
	}
	return duration, true, nil
}

// Adds a new travel time. If the coordinates don't already exist, it creates them first.
// If an entry with the precise origin and target coordinates already exists, skips it
func (db *DB) AddTravelTime(originCoord, targetCoord *geomath.Coord, duration int, distance int, mode string, departure_time int) error {
	// Get coordinate IDs
	originCoordID, err := db.getOrAddCoordID(originCoord)
	if err != nil {
		return err
	}
	targetCoordID, err := db.getOrAddCoordID(targetCoord)
	if err != nil {
		return err
	}

	// Add new travel time
	query := `INSERT INTO travel_times (origin_coord, target_coord, duration, distance, mode, departure_time) VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT DO NOTHING;`
	_, err = db.db.Exec(query, originCoordID, targetCoordID, duration, distance, mode, departure_time)
	if err != nil {
		return err
	}
	return nil
}

// Get the coordinate id in the database. If it doesn't exist, will create it
func (db *DB) getOrAddCoordID(coord *geomath.Coord) (int, error) {
	existsCoordID, err := db.ExistsCoordID(coord)
	if err != nil {
		return 0, err
	}
	if !existsCoordID {
		err := db.AddCoord(coord)
		if err != nil {
			return 0, err
		}
	}
	originCoordID, err := db.GetCoordID(coord)
	if err != nil {
		return 0, err
	}
	return originCoordID, nil

}

func (db *DB) ExistsCoordID(coord *geomath.Coord) (bool, error) {
	query := `SELECT FROM coords WHERE lat = $1 AND lng = $2;`
	err := db.db.QueryRow(query, coord.Lat, coord.Lng).Scan()
	if err != nil {
		if err != sql.ErrNoRows {
			// a real error happened!
			return false, err
		}

		return false, nil
	}
	return true, nil
}

func (db *DB) GetCoordID(coord *geomath.Coord) (int, error) {
	var id int
	query := `SELECT id FROM coords WHERE lat = $1 AND lng = $2;`
	err := db.db.QueryRow(query, coord.Lat, coord.Lng).Scan(&id)
	if err != nil {
		return 0, fmt.Errorf("could not find coordinate %v in database", coord.ToString())
	}
	return id, nil
}

func (db *DB) GetCoord(coordId int) (*geomath.Coord, error) {
	var lat, lng float64
	query := `SELECT coords.lat, coords.lng FROM coords WHERE id = $1;`
	err := db.db.QueryRow(query, coordId).Scan(&lat, &lng)
	if err != nil {
		logrus.Error(err.Error())
		return nil, fmt.Errorf("could not find coordinate with id %v in database", coordId)
	}
	return geomath.NewCoord(lat, lng), nil
}

// Insert a new coordinate, if it already exists, skips it
func (db *DB) AddCoord(coord *geomath.Coord) error {
	logrus.Debugf("Store coordinate %v", coord.ToString())
	query := `INSERT INTO coords (lat, lng) VALUES ($1, $2) ON CONFLICT DO NOTHING;`
	_, err := db.db.Exec(query, coord.Lat, coord.Lng)
	if err != nil {
		return err
	}
	return nil
}

// ---------------- Manage database itself ----------------
func connectDatabase(dbUri string) *sql.DB {
	db, err := sql.Open("postgres", dbUri)
	if err != nil {
		logrus.Fatal(err)
	}
	return db
}

func (db *DB) Close() {
	db.db.Close()
}
