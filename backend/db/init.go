package db

import (
	"database/sql"

	_ "github.com/lib/pq"

	"github.com/sirupsen/logrus"
)

// coords -       contains all coordinates used in any table
// travel_times - contains information of travel time between two coordinates. It stores context such as travel mode or departure time
// origins      - contains points with human readable names, represents all origins travel time heat maps can be generated from
func initDatabase(db *sql.DB) {
	query := `
		CREATE TABLE IF NOT EXISTS coords(
		    id serial PRIMARY KEY,
			lat float NOT NULL,
			lng float NOT NULL,
			UNIQUE(lat, lng)
		);

		CREATE TABLE IF NOT EXISTS travel_times(
		    id serial PRIMARY KEY,
		    origin_coord integer NOT NULL REFERENCES coords(id),
		    target_coord integer NOT NULL REFERENCES coords(id),
			duration int NOT NULL,
			distance int NOT NULL,
			mode text NOT NULL,
			departure_time bigint NOT NULL,
			UNIQUE(origin_coord, target_coord, mode)
		);

		CREATE TABLE IF NOT EXISTS origins(
		    id serial PRIMARY KEY,
		    origin_coord integer NOT NULL REFERENCES coords(id),
			name text NOT NULL,
			mode text NOT NULL,
			grid_radius integer NOT NULL,
			grid_stepsize integer NOT NULL,
			UNIQUE(origin_coord, name, mode, grid_radius, grid_stepsize)
		);`

	_, err := db.Exec(query)
	if err != nil {
		logrus.Fatal(err)
	}
}
